#ifndef __ANIMATION__
#define __ANIMATION__

#include <SFML/Graphics.hpp>

class Animation
{
public:
    enum Mask
    {
        AM_NONE = 0,
        AM_POSITION = 1 << 0,
        AM_ROTATION = 1 << 1,
        AM_SCALE = 1 << 2,
        AM_ORIGIN = 1 << 3,
        AM_ALL = -1
    };

    struct State
    {
        State()
        : position( sf::Vector2f( 0.0f, 0.0f ) )
        , rotation( 0.0f )
        , scale( sf::Vector2f( 1.0f, 1.0f ) )
        , origin( sf::Vector2f( 0.0f, 0.0f ) )
        {};
        State( const sf::Transformable& t )
        : position( t.getPosition() )
        , rotation( t.getRotation() )
        , scale( t.getScale() )
        , origin( t.getOrigin() )
        {};

        sf::Vector2f position;
        float rotation;
        sf::Vector2f scale;
        sf::Vector2f origin;
    };

    Animation( sf::Transformable* target_, const State& from_, const State& to_, float delay_, float duration_, Mask mask_ = AM_ALL )
    : target( target_ )
    , from( from_ )
    , to( to_ )
    , delay( delay_ )
    , duration( duration_ )
    , mask( mask_ )
    {};

    void apply()
    {
        if( target && mask && delay <= 0.0f )
        {
            float f = duration > 0.0f ? CLAMP( -delay / duration, 0.0f, 1.0f ) : 1.0f;
            if( mask & AM_POSITION )
                target->setPosition( sf::Vector2f( LERP( f, from.position.x, to.position.x ), LERP( f, from.position.y, to.position.y ) ) );
            if( mask & AM_ROTATION )
                target->setRotation( LERP( f, from.rotation, to.rotation ) );
            if( mask & AM_SCALE )
                target->setScale( sf::Vector2f( LERP( f, from.scale.x, to.scale.x ), LERP( f, from.scale.y, to.scale.y ) ) );
            if( mask & AM_ORIGIN )
                target->setOrigin( sf::Vector2f( LERP( f, from.origin.x, to.origin.x ), LERP( f, from.origin.y, to.origin.y ) ) );
        }
    }

    sf::Transformable* target;
    State from;
    State to;
    float delay;
    float duration;
    Mask mask;
};

#endif
