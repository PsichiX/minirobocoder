#include "Bullet.h"

extern int g_turnTimeMs;
extern sf::Texture* g_textureBullet;

Bullet::Bullet( Robot* creator, map< string, Robot* >& robots, const sf::Vector2i& pos, float dir )
: m_creator( creator )
, m_robots( &robots )
, m_shape( 0 )
, m_shapeDebug( 0 )
, m_alive( true )
{
    setPosition( pos );
    m_shape = xnew sf::RectangleShape();
    m_shape->setTexture( g_textureBullet );
    m_shape->setSize( sf::Vector2f( 1.0f, 0.5f ) );
    m_shape->setOrigin( 0.5f, 0.25f );
    m_shape->setPosition( (float)pos.x, (float)pos.y );
    float rad = DEGTORAD( dir );
    m_velocity = sf::Vector2i( (int)roundf( cosf( rad ) ), (int)roundf( sinf( rad ) ) );
    m_shape->setRotation( dir );
    m_shapeDebug = xnew sf::RectangleShape();
    m_shapeDebug->setOutlineColor( sf::Color( 255, 255, 0, 192 ) );
    m_shapeDebug->setOutlineThickness( 0.05f );
    m_shapeDebug->setFillColor( sf::Color( 0, 0, 0, 0 ) );
    m_shapeDebug->setSize( sf::Vector2f( 1.0f, 1.0f ) );
    m_shapeDebug->setOrigin( 0.5f, 0.5f );
    m_shapeDebug->setPosition( (float)pos.x, (float)pos.y );
}

Bullet::~Bullet()
{
    DELETE_OBJECT( m_shape );
}

map< string, Robot* >* Bullet::getRobots()
{
    return m_robots;
}

sf::RectangleShape* Bullet::getShape()
{
    return m_shape;
}

sf::RectangleShape* Bullet::getShapeDebug()
{
    return m_shapeDebug;
}

bool Bullet::isAlive()
{
    return m_alive;
}

sf::Vector2i Bullet::getPosition()
{
    return m_position;
}

void Bullet::setPosition( const sf::Vector2i& p )
{
    m_position = p;
}

float Bullet::getDirection()
{
    return m_shape ? m_shape->getRotation() : 0.0f;
}

void Bullet::processStep( const sf::IntRect& worldRect, list< Animation* >& anims )
{
    if( m_robots )
    {
        for( map< string, Robot* >::iterator it = m_robots->begin(); it != m_robots->end(); it++ )
        {
            if( it->second == 0 || it->second == m_creator )
                continue;
            if( it->second->getTargetPosition() == m_position )
            {
                kill();
                it->second->damage( 1 );
            };
        }
    }
    sf::Vector2i tp = m_position;
    Animation::State animFrom;
    animFrom.position = sf::Vector2f( (float)tp.x, (float)tp.y );
    m_shape->setPosition( (float)m_position.x, (float)m_position.y );
    m_position += m_velocity;
    m_shapeDebug->setPosition( (float)m_position.x, (float)m_position.y );
    Animation::State animTo;
    tp = m_position;
    animTo.position = sf::Vector2f( (float)tp.x, (float)tp.y );
    if( m_position.x < worldRect.left ||
        m_position.x > worldRect.left + worldRect.width ||
        m_position.y < worldRect.top ||
        m_position.y > worldRect.top + worldRect.height )
        kill();
    if( m_alive )
        anims.push_back( xnew Animation( m_shape, animFrom, animTo, 0.0f, (float)g_turnTimeMs * 0.001f, Animation::AM_POSITION ) );
}

void Bullet::kill()
{
    m_alive = false;
}
