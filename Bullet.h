#ifndef __BULLET__
#define __BULLET__

#include <windows.h>
#include <iostream>
#include <SFML/Graphics.hpp>
#include "Robot.h"
#include "Animation.h"

using namespace std;

class Bullet
{
public:
    Bullet( Robot* creator, map< string, Robot* >& robots, const sf::Vector2i& pos, float dir );
    ~Bullet();
    map< string, Robot* >* getRobots();
    sf::RectangleShape* getShape();
    sf::RectangleShape* getShapeDebug();
    bool isAlive();
    sf::Vector2i getPosition();
    void setPosition( const sf::Vector2i& p );
    float getDirection();
    void processStep( const sf::IntRect& worldRect, list< Animation* >& anims );
    void kill();

private:
    Robot* m_creator;
    map< string, Robot* >* m_robots;
    sf::RectangleShape* m_shape;
    sf::RectangleShape* m_shapeDebug;
    sf::Vector2i m_position;
    sf::Vector2i m_velocity;
    bool m_alive;
};

#endif
