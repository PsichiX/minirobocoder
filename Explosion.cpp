#include "Explosion.h"

extern sf::Texture* g_textureExplosion;

Explosion::Explosion( const sf::Vector2i& pos, float duration )
: m_shape( 0 )
, m_shapeDebug( 0 )
, m_frames( 1 )
, m_time( 0.0f )
, m_duration( duration )
{
    m_shape = xnew sf::RectangleShape();
    m_shape->setTexture( g_textureExplosion );
    m_shape->setSize( sf::Vector2f( 2.0f, 2.0f ) );
    m_shape->setOrigin( 1.0f, 1.0f );
    m_shape->setPosition( (float)pos.x, (float)pos.y );
    m_shapeDebug = xnew sf::RectangleShape();
    m_shapeDebug->setOutlineColor( sf::Color( 255, 255, 255, 192 ) );
    m_shapeDebug->setOutlineThickness( 0.05f );
    m_shapeDebug->setFillColor( sf::Color( 0, 0, 0, 0 ) );
    m_shapeDebug->setSize( sf::Vector2f( 2.0f, 2.0f ) );
    m_shapeDebug->setOrigin( 1.0f, 1.0f );
    m_shapeDebug->setPosition( (float)pos.x, (float)pos.y );
    m_frames = 5;
    animate( 0.0f );
}

Explosion::~Explosion()
{
    DELETE_OBJECT( m_shape );
    DELETE_OBJECT( m_shapeDebug );
}

sf::RectangleShape* Explosion::getShape()
{
    return m_shape;
}

sf::RectangleShape* Explosion::getShapeDebug()
{
    return m_shapeDebug;
}

void Explosion::animate( float dt )
{
    m_time += dt;
    m_frames = max( 1u, m_frames );
    unsigned int frame = (unsigned int)floorf( (float)m_frames * ( m_duration > 0.0f ? ( m_time / m_duration ) : 1.0f ) );
    sf::Vector2u ts = g_textureExplosion->getSize();
    sf::IntRect rect;
    rect.width = ts.x / m_frames;
    rect.height = ts.y;
    rect.left = rect.width * frame;
    rect.top = 0;
    m_shape->setTextureRect( rect );
}

bool Explosion::isComplete()
{
    return m_time >= m_duration;
}
