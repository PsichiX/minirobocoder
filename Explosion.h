#ifndef __EXPLOSION__
#define __EXPLOSION__

#include <algorithm>
#include <XeCore/Common/MemoryManager.h>
#include <SFML/Graphics.hpp>

using namespace std;
using namespace XeCore;
using namespace XeCore::Common;

class Explosion
{
public:
    Explosion( const sf::Vector2i& pos, float duration );
    ~Explosion();
    sf::RectangleShape* getShape();
    sf::RectangleShape* getShapeDebug();
    void animate( float dt );
    bool isComplete();

private:
    sf::RectangleShape* m_shape;
    sf::RectangleShape* m_shapeDebug;
    unsigned int m_frames;
    float m_time;
    float m_duration;
};

#endif
