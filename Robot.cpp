#include "Robot.h"
#include <sstream>
#include "Utils.h"

extern int g_turnTimeMs;
extern sf::Font* g_font;
extern sf::Texture* g_textureRobot;

Robot::Robot( string fname, IntuicioVM* vm, map< string, Robot* >& robots, const sf::Vector2i& pos, float dir  )
: m_name( fname )
, m_program( 0 )
, m_vm( vm )
, m_context( 0 )
, m_intercepts( 0 )
, m_entryStart( MAXDWORD )
, m_entryStep( MAXDWORD )
, m_entryStop( MAXDWORD )
, m_robots( &robots )
, m_shape( 0 )
, m_shapeDebug( 0 )
, m_text( 0 )
, m_bullets( 10 )
, m_shields( 3 )
, m_canShoot( false )
{
    if( !vm )
    {
        cout << "Intuicio Virtual Machine cannot be null!" << endl;
        return;
    }
    if( fname.substr( fname.length() - 4 ) == ".isc" )
    {
        String str;
        stringstream ss;
        ss << "-i " << "'robots/" << fname << "' -o 'robots/" << fname << ".itc' -m 'robots/" << fname << ".imm' -sd 'sdk'";
        if( !compile( ss.str(), str ) )
        {
            cout << "Cannot compile Intuicio script: " << fname << endl;
            cout << str;
            return;
        }
        fname += ".itc";
    }
    ifstream file( ( string( "robots/" ) + fname ).c_str(), std::ifstream::in | std::ofstream::binary  );
    if( !file )
    {
        cout << "Cannot open file: " << fname << endl;
        return;
    }
    file.seekg( 0, ifstream::end );
    unsigned int fsize = file.tellg();
    if( !fsize )
    {
        cout << "Empty file: " << fname << endl;
        file.close();
        return;
    }
    file.seekg( 0, ifstream::beg );
    m_program = xnew ProgramVM();
    m_program->getBuffer()->resize( fsize );
    file.read( (char*)m_program->getBuffer()->data(), fsize );
    file.close();
    if( !m_program->validate() )
    {
        cout << "Invalid program: " << fname << endl;
        DELETE_OBJECT( m_program );
        return;
    }
    map< String, ProgramVM::Pointer > exps;
    m_program->getExports( exps );
    for( map< String, ProgramVM::Pointer >::iterator it = exps.begin(); it != exps.end(); it++ )
    {
        if( it->first == "ENTRY_START" )
            m_entryStart = it->second.address;
        else if( it->first == "ENTRY_STEP" )
            m_entryStep = it->second.address;
        else if( it->first == "ENTRY_STOP" )
            m_entryStop = it->second.address;
    }
    if( m_entryStart == MAXDWORD )
    {
        cout << "Start entry not found for: " << fname << endl;
        DELETE_OBJECT( m_program );
        return;
    }
    if( m_entryStep == MAXDWORD )
    {
        cout << "Step entry not found for: " << fname << endl;
        DELETE_OBJECT( m_program );
        return;
    }
    if( m_entryStop == MAXDWORD )
    {
        cout << "Stop entry not found for: " << fname << endl;
        DELETE_OBJECT( m_program );
        return;
    }
    m_context = vm->createContext( m_program, fname.c_str() );
    if( !m_context )
    {
        cout << "Invalid program context: " << fname << endl;
        DELETE_OBJECT( m_program );
        return;
    }
    m_intercepts = xnew RobotInterceptions( this, m_context );
    if( !m_context->registerInterceptionListener( "RobotInterceptions", m_intercepts ) )
    {
        cout << "Cannot register interceptions for: " << fname << endl;
        m_vm->destroyContext( m_context );
        DELETE_OBJECT( m_program );
        return;
    }
    resetPosition( pos );
    m_shape = xnew sf::RectangleShape();
    m_shape->setTexture( g_textureRobot );
    m_shape->setSize( sf::Vector2f( 1.0f, 1.0f ) );
    m_shape->setOrigin( 0.5f, 0.5f );
    m_shape->setPosition( (float)pos.x, (float)pos.y );
    m_shapeDebug = xnew sf::RectangleShape();
    m_shapeDebug->setOutlineColor( sf::Color( 255, 0, 0, 192 ) );
    m_shapeDebug->setOutlineThickness( 0.05f );
    m_shapeDebug->setFillColor( sf::Color( 0, 0, 0, 0 ) );
    m_shapeDebug->setSize( sf::Vector2f( 1.0f, 1.0f ) );
    m_shapeDebug->setOrigin( 0.5f, 0.5f );
    m_shapeDebug->setPosition( (float)pos.x, (float)pos.y );
    m_text = xnew sf::Text();
    m_text->setFont( *g_font );
    m_text->setCharacterSize( 36 );
    m_text->setStyle( sf::Text::Bold );
    m_text->setString( fname );
    m_text->setColor( sf::Color::White );
}

Robot::~Robot()
{
    if( m_context )
        m_context->unregisterAllInterceptionListeners();
    DELETE_OBJECT( m_intercepts );
    if( m_vm )
        m_vm->destroyContext( m_context );
    DELETE_OBJECT( m_program );
    DELETE_OBJECT( m_shape );
    DELETE_OBJECT( m_shapeDebug );
    DELETE_OBJECT( m_text );
}

const string& Robot::name()
{
    return m_name;
}

ProgramVM* Robot::getProgram()
{
    return m_program;
}

IntuicioVM* Robot::getVM()
{
    return m_vm;
}

ContextVM* Robot::getContext()
{
    return m_context;
}

map< string, Robot* >* Robot::getRobots()
{
    return m_robots;
}

sf::RectangleShape* Robot::getShape()
{
    return m_shape;
}

sf::RectangleShape* Robot::getShapeDebug()
{
    return m_shapeDebug;
}

sf::Text* Robot::getText()
{
    return m_text;
}

bool Robot::isValid()
{
    return  m_program &&
            m_vm &&
            m_context &&
            m_entryStart != MAXDWORD &&
            m_entryStep != MAXDWORD &&
            m_entryStop != MAXDWORD &&
            m_shape &&
            m_text;
}

void Robot::processStart()
{
    if( m_context && m_entryStart != MAXDWORD )
        m_context->runProgram( true, m_entryStart );
}

void Robot::processStep( const sf::IntRect& worldRect, list< Animation* >& anims )
{
    resetPosition( m_targetPosition );
    m_shape->setPosition( (float)m_targetPosition.x, (float)m_targetPosition.y );
    m_text->setPosition( (float)m_targetPosition.x, (float)m_targetPosition.y );
    if( m_context && m_entryStep != MAXDWORD )
        m_context->runProgram( true, m_entryStep );
    m_sourcePosition.x = CLAMP( m_sourcePosition.x, worldRect.left, worldRect.left + worldRect.width );
    m_sourcePosition.y = CLAMP( m_sourcePosition.y, worldRect.top, worldRect.top + worldRect.height );
    m_targetPosition.x = CLAMP( m_targetPosition.x, worldRect.left, worldRect.left + worldRect.width );
    m_targetPosition.y = CLAMP( m_targetPosition.y, worldRect.top, worldRect.top + worldRect.height );
    Animation::State animFrom;
    sf::Vector2i tp = m_sourcePosition;
    animFrom.position = sf::Vector2f( (float)tp.x, (float)tp.y );
    sf::Vector2i dp = m_targetPosition - m_sourcePosition;
    if( ( dp.x * dp.x ) + ( dp.y * dp.y ) > 0.5 )
        m_shape->setRotation( RADTODEG( atan2f( (float)dp.y, (float)dp.x ) ) );
    if( isCollide() )
        idle();
    Animation::State animTo;
    tp = m_targetPosition;
    animTo.position = sf::Vector2f( (float)tp.x, (float)tp.y );
    m_shapeDebug->setPosition( (float)m_targetPosition.x, (float)m_targetPosition.y );
    if( m_shields > 0 )
        anims.push_back( xnew Animation( m_shape, animFrom, animTo, 0.0f, (float)g_turnTimeMs *  0.001f, Animation::AM_POSITION ) );
}

void Robot::processStop()
{
    if( m_context && m_entryStop != MAXDWORD )
        m_context->runProgram( true, m_entryStop );
}

bool Robot::isAlive()
{
    return m_shields > 0;
}

sf::Vector2i Robot::getSourcePosition()
{
    return m_sourcePosition;
}

sf::Vector2i Robot::getTargetPosition()
{
    return m_targetPosition;
}

sf::Vector2i Robot::getForwardPosition()
{
    float rad = DEGTORAD( m_shape->getRotation() );
    sf::Vector2i d = sf::Vector2i( (int)roundf( cosf( rad ) ), (int)roundf( sinf( rad ) ) );
    return m_targetPosition + d;
}

void Robot::setSourcePosition( const sf::Vector2i& p )
{
    m_sourcePosition = p;
}

void Robot::setTargetPosition( const sf::Vector2i& p )
{
    m_targetPosition = p;
}

void Robot::resetPosition( const sf::Vector2i& p )
{
    setSourcePosition( p );
    setTargetPosition( p );
}

float Robot::getDirection()
{
    return m_shape->getRotation();
}

bool Robot::isCollide()
{
    if( !m_robots )
        return false;
    for( map< string, Robot* >::iterator it = m_robots->begin(); it != m_robots->end(); it++ )
    {
        if( it->second == 0 || it->second == this )
            continue;
        if( it->second->getTargetPosition() == m_targetPosition )
            return true;
    }
    return false;
}

int Robot::getBulletsCount()
{
    return m_bullets;
}

int Robot::getShieldsCount()
{
    return m_shields;
}

bool Robot::canShoot()
{
    return m_canShoot;
}

void Robot::damage( int v )
{
    m_shields -= v;
}

void Robot::shoot()
{
    m_canShoot = false;
    m_bullets--;
}

void Robot::idle( Robot::Direction dir )
{
    resetPosition( m_sourcePosition );
    m_canShoot = false;
    if( dir == Robot::D_LEFT )
        m_shape->setRotation( 180.0f );
    else if( dir == Robot::D_UP )
        m_shape->setRotation( 90.0f );
    else if( dir == Robot::D_RIGHT )
        m_shape->setRotation( 0.0f );
    else if( dir == Robot::D_DOWN )
        m_shape->setRotation( 270.0f );
}

void Robot::goLeft()
{
    sf::Vector2i p = m_targetPosition;
    setSourcePosition( p );
    p.x--;
    setTargetPosition( p );
    m_canShoot = false;
}

void Robot::goUp()
{
    sf::Vector2i p = m_targetPosition;
    setSourcePosition( p );
    p.y--;
    setTargetPosition( p );
    m_canShoot = false;
}

void Robot::goRight()
{
    sf::Vector2i p = m_targetPosition;
    setSourcePosition( p );
    p.x++;
    setTargetPosition( p );
    m_canShoot = false;
}

void Robot::goDown()
{
    sf::Vector2i p = m_targetPosition;
    setSourcePosition( p );
    p.y++;
    setTargetPosition( p );
    m_canShoot = false;
}

void Robot::fire()
{
    m_canShoot = m_bullets > 0;
    resetPosition( m_sourcePosition );
}

Robot::Config Robot::Config::fromString( const String& str )
{
    Robot::Config c;
    unsigned int partsc = 0;
    String* parts = str.explode( ";", partsc, false, 0, "\"'" );
    if( !parts || !partsc )
    {
        DELETE_ARRAY( parts );
        return c;
    }
    if( partsc > 0 )
        c.file.assign( parts[ 0 ].c_str() );
    if( partsc > 1 )
    {
        unsigned int rpartsc = 0;
        String* rparts = parts[ 1 ].explode( "<>", rpartsc );
        if( rparts && rpartsc == 2 )
        {
            int a = 0;
            rparts[ 0 ].convertTo< int >( a );
            int b = 0;
            rparts[ 1 ].convertTo< int >( b );
            c.startX = random( a, b );
        }
        else
            parts[ 1 ].convertTo< int >( c.startX );
        DELETE_ARRAY( rparts );
    }
    if( partsc > 2 )
    {
        unsigned int rpartsc = 0;
        String* rparts = parts[ 2 ].explode( "<>", rpartsc );
        if( rparts && rpartsc == 2 )
        {
            int a = 0;
            rparts[ 0 ].convertTo< int >( a );
            int b = 0;
            rparts[ 1 ].convertTo< int >( b );
            c.startY = random( a, b );
        }
        else
            parts[ 2 ].convertTo< int >( c.startY );
        DELETE_ARRAY( rparts );
    }
    return c;
}

string Robot::Config::toString()
{
    stringstream ss;
    ss << "File: " << file << "; Start X: " << startX << "; Start Y: " << startY;
    return ss.str();
}
