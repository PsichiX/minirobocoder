#ifndef __ROBOT__
#define __ROBOT__

#include <windows.h>
#include <iostream>
#include <fstream>
#include <map>
#include <SFML/Graphics.hpp>
#include <XeCore/Common/String.h>
#include <XeCore/Intuicio/IntuicioVM.h>
#include <IntuicioCompiler.h>
#include "RobotInterceptions.h"
#include "Animation.h"

using namespace std;
using namespace XeCore;
using namespace XeCore::Common;
using namespace XeCore::Intuicio;

class RobotInterceptions;

class Robot
{
public:
    enum Direction
    {
        D_NONE,
        D_LEFT,
        D_UP,
        D_RIGHT,
        D_DOWN
    };

    Robot( string fname, IntuicioVM* vm, map< string, Robot* >& robots, const sf::Vector2i& pos, float dir );
    ~Robot();
    const string& name();
    ProgramVM* getProgram();
    IntuicioVM* getVM();
    ContextVM* getContext();
    map< string, Robot* >* getRobots();
    sf::RectangleShape* getShape();
    sf::RectangleShape* getShapeDebug();
    sf::Text* getText();
    bool isValid();
    void processStart();
    void processStep( const sf::IntRect& worldRect, list< Animation* >& anims );
    void processStop();
    bool isAlive();
    sf::Vector2i getSourcePosition();
    sf::Vector2i getTargetPosition();
    sf::Vector2i getForwardPosition();
    void setSourcePosition( const sf::Vector2i& p );
    void setTargetPosition( const sf::Vector2i& p );
    void resetPosition( const sf::Vector2i& p );
    float getDirection();
    bool isCollide();
    int getBulletsCount();
    int getShieldsCount();
    bool canShoot();
    void damage( int v );
    void shoot();
    void kill();
    void idle( Direction dir = D_NONE );
    void goLeft();
    void goUp();
    void goRight();
    void goDown();
    void fire();

private:
    string m_name;
    ProgramVM* m_program;
    IntuicioVM* m_vm;
    ContextVM* m_context;
    ContextVM::OnInterceptListener* m_intercepts;
    unsigned int m_entryStart;
    unsigned int m_entryStep;
    unsigned int m_entryStop;
    map< string, Robot* >* m_robots;
    sf::RectangleShape* m_shape;
    sf::RectangleShape* m_shapeDebug;
    sf::Text* m_text;
    sf::Vector2i m_sourcePosition;
    sf::Vector2i m_targetPosition;
    int m_bullets;
    int m_shields;
    bool m_canShoot;

public:
    struct Config
    {
        string file;
        int startX;
        int startY;

        Config() : file( "" ), startX( 0 ), startY( 0 ) {};
        Config( string& f, int x, int y ) : file( f ), startX( x ), startY( y ) {};
        static Config fromString( const String& str );
        string toString();
    };
};

#endif
