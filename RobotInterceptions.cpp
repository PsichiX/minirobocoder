#include "RobotInterceptions.h"

RobotInterceptions::RobotInterceptions( Robot* owner, ContextVM* context )
: m_owner( owner )
, m_context( context )
{
}

RobotInterceptions::~RobotInterceptions()
{
};

bool RobotInterceptions::onIntercept( ParallelThreadVM* caller, unsigned int code )
{
    if( !m_owner || !m_context )
        return true;
    if( code == 0 )
    {
        Robot::Direction d = Robot::D_NONE;
        if( !m_context->stackPop( caller, &d, sizeof( int ) ) )
            return true;
        m_owner->idle( d );
        return false;
    }
    else if( code == 1 )
    {
        m_owner->goLeft();
        return false;
    }
    else if( code == 2 )
    {
        m_owner->goUp();
        return false;
    }
    else if( code == 3 )
    {
        m_owner->goRight();
        return false;
    }
    else if( code == 4 )
    {
        m_owner->goDown();
        return false;
    }
    else if( code == 5 )
    {
        m_owner->fire();
        return false;
    }
    else if( code == 6 )
    {
        int p = m_owner->getSourcePosition().x;
        if( !m_context->stackPush( caller, &p, sizeof( int ) ) )
            return true;
        return false;
    }
    else if( code == 7 )
    {
        int p = m_owner->getSourcePosition().y;
        if( !m_context->stackPush( caller, &p, sizeof( int ) ) )
            return true;
        return false;
    }
    else if( code == 8 )
    {
        int dir = (int)m_owner->getDirection() % 360;
        Robot::Direction d = Robot::D_NONE;
        if( dir > 180 - 45 && dir <= 180 + 45 )
            d = Robot::D_LEFT;
        else if( dir > 270 - 45 && dir <= 270 + 45 )
            d = Robot::D_UP;
        else if( dir > 360 - 45 || dir <= 0 + 45 )
            d = Robot::D_RIGHT;
        else if( dir > 90 - 45 && dir <= 90 + 45 )
            d = Robot::D_DOWN;
        if( !m_context->stackPush( caller, &d, sizeof( int ) ) )
            return true;
        return false;
    }
    else if( code == 9 )
    {
        int c = m_owner->getBulletsCount();
        if( !m_context->stackPush( caller, &c, sizeof( int ) ) )
            return true;
        return false;
    }
    else if( code == 10 )
    {
        int c = m_owner->getShieldsCount();
        if( !m_context->stackPush( caller, &c, sizeof( int ) ) )
            return true;
        return false;
    }
    else if( code == 11 )
    {
        int c = 0;
        for( map< string, Robot* >::iterator it = m_owner->getRobots()->begin(); it != m_owner->getRobots()->end(); it++ )
            if( it->second != m_owner )
                c++;
        if( !m_context->stackPush( caller, &c, sizeof( int ) ) )
            return true;
        return false;
    }
    else if( code == 12 )
    {
        int id = -1;
        if( !m_context->stackPop( caller, &id, sizeof( int ) ) )
            return true;
        int i = 0;
        for( map< string, Robot* >::iterator it = m_owner->getRobots()->begin(); it != m_owner->getRobots()->end(); it++, i++ )
        {
            if( it->second == m_owner )
            {
                i--;
                continue;
            }
            if( i == id )
            {
                int p = it->second->getSourcePosition().x;
                if( !m_context->stackPush( caller, &p, sizeof( int ) ) )
                    return true;
                return false;
            }
        }
        int p = 0;
        if( !m_context->stackPush( caller, &p, sizeof( int ) ) )
            return true;
        return false;
    }
    else if( code == 13 )
    {
        int id = -1;
        if( !m_context->stackPop( caller, &id, sizeof( int ) ) )
            return true;
        int i = 0;
        for( map< string, Robot* >::iterator it = m_owner->getRobots()->begin(); it != m_owner->getRobots()->end(); it++, i++ )
        {
            if( it->second == m_owner )
            {
                i--;
                continue;
            }
            if( i == id )
            {
                int p = it->second->getSourcePosition().y;
                if( !m_context->stackPush( caller, &p, sizeof( int ) ) )
                    return true;
                return false;
            }
        }
        int p = 0;
        if( !m_context->stackPush( caller, &p, sizeof( int ) ) )
            return true;
        return false;
    }
    else if( code == 14 )
    {
        int px = 0;
        if( !m_context->stackPop( caller, &px, sizeof( int ) ) )
            return true;
        int py = 0;
        if( !m_context->stackPop( caller, &py, sizeof( int ) ) )
            return true;
        int i = 0;
        for( map< string, Robot* >::iterator it = m_owner->getRobots()->begin(); it != m_owner->getRobots()->end(); it++, i++ )
        {
            if( it->second == m_owner )
            {
                i--;
                continue;
            }
            sf::Vector2i p = it->second->getSourcePosition();
            if( p.x == px && p.y == py )
            {
                if( !m_context->stackPush( caller, &i, sizeof( int ) ) )
                    return true;
                return false;
            }
        }
        i = -1;
        if( !m_context->stackPush( caller, &i, sizeof( int ) ) )
            return true;
        return false;
    }
    else if( code == 15 )
    {
        int id = -1;
        if( !m_context->stackPop( caller, &id, sizeof( int ) ) )
            return true;
        int i = 0;
        Robot* r = 0;
        Robot::Direction d = Robot::D_NONE;
        for( map< string, Robot* >::iterator it = m_owner->getRobots()->begin(); it != m_owner->getRobots()->end(); it++, i++ )
        {
            if( it->second == m_owner )
            {
                i--;
                continue;
            }
            if( i == id )
                r = it->second;
        }
        if( r )
        {
            int dir = (int)r->getDirection() % 360;
            if( dir > 180 - 45 && dir <= 180 + 45 )
                d = Robot::D_LEFT;
            else if( dir > 270 - 45 && dir <= 270 + 45 )
                d = Robot::D_UP;
            else if( dir > 360 - 45 || dir <= 0 + 45 )
                d = Robot::D_RIGHT;
            else if( dir > 90 - 45 && dir <= 90 + 45 )
                d = Robot::D_DOWN;
        }
        if( !m_context->stackPush( caller, &d, sizeof( int ) ) )
            return true;
        return false;
    }
    return true;
};
