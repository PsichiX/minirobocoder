#ifndef __ROBOT_INTERCEPTIONS__
#define __ROBOT_INTERCEPTIONS__

#include <windows.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <SFML/Graphics.hpp>
#include <XeCore/Intuicio/IntuicioVM.h>
#include <IntuicioCompiler.h>
#include "Robot.h"

using namespace std;
using namespace XeCore;
using namespace XeCore::Common;
using namespace XeCore::Intuicio;

class Robot;

class RobotInterceptions : public ContextVM::OnInterceptListener
{
public:
    RobotInterceptions( Robot* owner, ContextVM* context );
    virtual ~RobotInterceptions();
    virtual bool onIntercept( ParallelThreadVM* caller, unsigned int code );

private:
    Robot* m_owner;
    ContextVM* m_context;
};

#endif
