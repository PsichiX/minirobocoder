#include <cstdlib>
#include <algorithm>
#include <iostream>
#include "Utils.h"

using namespace std;

void randomSeed( unsigned int s )
{
    srand( s );
}

int random( int mn, int mx )
{
    int a = min( mn, mx );
    int b = max( mn, mx );
    return rand() % ( b - a ) + a;
}
