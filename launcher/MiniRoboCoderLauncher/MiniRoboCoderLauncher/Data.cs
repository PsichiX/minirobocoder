﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MiniRoboCoderLauncher
{
    public class Data
    {
        public bool DebugMode;
        public bool WriteLogOutput;
        public int TurnDurationMs;
        public bool ForcedRandomSeed;
        public int RandomSeed;
        public List<Robot> Robots;
    }
}
