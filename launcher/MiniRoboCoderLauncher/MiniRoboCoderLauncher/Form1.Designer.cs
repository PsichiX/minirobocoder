﻿namespace MiniRoboCoderLauncher
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.checkBoxForcedSeed = new System.Windows.Forms.CheckBox();
            this.numericRandomSeed = new System.Windows.Forms.NumericUpDown();
            this.numericTurnTime = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBoxWriteLog = new System.Windows.Forms.CheckBox();
            this.checkBoxDebugMode = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.buttonRemoveRobot = new System.Windows.Forms.Button();
            this.buttonAddRobot = new System.Windows.Forms.Button();
            this.listBoxRobots = new System.Windows.Forms.ListBox();
            this.buttonLaunch = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericRandomSeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericTurnTime)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.checkBoxForcedSeed);
            this.groupBox1.Controls.Add(this.numericRandomSeed);
            this.groupBox1.Controls.Add(this.numericTurnTime);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.checkBoxWriteLog);
            this.groupBox1.Controls.Add(this.checkBoxDebugMode);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(296, 158);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Simulation properties";
            // 
            // checkBoxForcedSeed
            // 
            this.checkBoxForcedSeed.AutoSize = true;
            this.checkBoxForcedSeed.Location = new System.Drawing.Point(7, 107);
            this.checkBoxForcedSeed.Name = "checkBoxForcedSeed";
            this.checkBoxForcedSeed.Size = new System.Drawing.Size(153, 17);
            this.checkBoxForcedSeed.TabIndex = 8;
            this.checkBoxForcedSeed.Text = "Forced randomization seed";
            this.checkBoxForcedSeed.UseVisualStyleBackColor = true;
            this.checkBoxForcedSeed.CheckedChanged += new System.EventHandler(this.checkBoxForcedSeed_CheckedChanged);
            // 
            // numericRandomSeed
            // 
            this.numericRandomSeed.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.numericRandomSeed.Enabled = false;
            this.numericRandomSeed.Location = new System.Drawing.Point(7, 130);
            this.numericRandomSeed.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericRandomSeed.Name = "numericRandomSeed";
            this.numericRandomSeed.ReadOnly = true;
            this.numericRandomSeed.Size = new System.Drawing.Size(283, 20);
            this.numericRandomSeed.TabIndex = 7;
            // 
            // numericTurnTime
            // 
            this.numericTurnTime.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.numericTurnTime.Location = new System.Drawing.Point(7, 80);
            this.numericTurnTime.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericTurnTime.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericTurnTime.Name = "numericTurnTime";
            this.numericTurnTime.Size = new System.Drawing.Size(283, 20);
            this.numericTurnTime.TabIndex = 6;
            this.numericTurnTime.Value = new decimal(new int[] {
            500,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(138, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Turn duration (milliseconds):";
            // 
            // checkBoxWriteLog
            // 
            this.checkBoxWriteLog.AutoSize = true;
            this.checkBoxWriteLog.Checked = true;
            this.checkBoxWriteLog.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxWriteLog.Location = new System.Drawing.Point(6, 43);
            this.checkBoxWriteLog.Name = "checkBoxWriteLog";
            this.checkBoxWriteLog.Size = new System.Drawing.Size(158, 17);
            this.checkBoxWriteLog.TabIndex = 2;
            this.checkBoxWriteLog.Text = "Write battle history to log file";
            this.checkBoxWriteLog.UseVisualStyleBackColor = true;
            // 
            // checkBoxDebugMode
            // 
            this.checkBoxDebugMode.AutoSize = true;
            this.checkBoxDebugMode.Location = new System.Drawing.Point(6, 19);
            this.checkBoxDebugMode.Name = "checkBoxDebugMode";
            this.checkBoxDebugMode.Size = new System.Drawing.Size(87, 17);
            this.checkBoxDebugMode.TabIndex = 1;
            this.checkBoxDebugMode.Text = "Debug mode";
            this.checkBoxDebugMode.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.buttonRemoveRobot);
            this.groupBox2.Controls.Add(this.buttonAddRobot);
            this.groupBox2.Controls.Add(this.listBoxRobots);
            this.groupBox2.Location = new System.Drawing.Point(13, 177);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(296, 178);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Battle robots";
            // 
            // buttonRemoveRobot
            // 
            this.buttonRemoveRobot.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonRemoveRobot.Location = new System.Drawing.Point(155, 149);
            this.buttonRemoveRobot.Name = "buttonRemoveRobot";
            this.buttonRemoveRobot.Size = new System.Drawing.Size(135, 23);
            this.buttonRemoveRobot.TabIndex = 2;
            this.buttonRemoveRobot.Text = "Remove robot";
            this.buttonRemoveRobot.UseVisualStyleBackColor = true;
            this.buttonRemoveRobot.Click += new System.EventHandler(this.buttonRemoveRobot_Click);
            // 
            // buttonAddRobot
            // 
            this.buttonAddRobot.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonAddRobot.Location = new System.Drawing.Point(7, 149);
            this.buttonAddRobot.Name = "buttonAddRobot";
            this.buttonAddRobot.Size = new System.Drawing.Size(138, 23);
            this.buttonAddRobot.TabIndex = 1;
            this.buttonAddRobot.Text = "Add robot";
            this.buttonAddRobot.UseVisualStyleBackColor = true;
            this.buttonAddRobot.Click += new System.EventHandler(this.buttonAddRobot_Click);
            // 
            // listBoxRobots
            // 
            this.listBoxRobots.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.listBoxRobots.FormattingEnabled = true;
            this.listBoxRobots.Location = new System.Drawing.Point(7, 20);
            this.listBoxRobots.Name = "listBoxRobots";
            this.listBoxRobots.Size = new System.Drawing.Size(283, 121);
            this.listBoxRobots.TabIndex = 0;
            // 
            // buttonLaunch
            // 
            this.buttonLaunch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonLaunch.Location = new System.Drawing.Point(12, 361);
            this.buttonLaunch.Name = "buttonLaunch";
            this.buttonLaunch.Size = new System.Drawing.Size(296, 23);
            this.buttonLaunch.TabIndex = 2;
            this.buttonLaunch.Text = "Launch";
            this.buttonLaunch.UseVisualStyleBackColor = true;
            this.buttonLaunch.Click += new System.EventHandler(this.buttonLaunch_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(321, 396);
            this.Controls.Add(this.buttonLaunch);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(329, 319);
            this.Name = "FormMain";
            this.Text = "MiniRoboCoder Launcher";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericRandomSeed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericTurnTime)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox checkBoxDebugMode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox checkBoxWriteLog;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button buttonRemoveRobot;
        private System.Windows.Forms.Button buttonAddRobot;
        private System.Windows.Forms.ListBox listBoxRobots;
        private System.Windows.Forms.Button buttonLaunch;
        private System.Windows.Forms.NumericUpDown numericRandomSeed;
        private System.Windows.Forms.NumericUpDown numericTurnTime;
        private System.Windows.Forms.CheckBox checkBoxForcedSeed;
    }
}

