﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using Newtonsoft.Json;

namespace MiniRoboCoderLauncher
{
    public partial class FormMain : Form
    {
        private List<Robot> mRobots = new List<Robot>();

        public FormMain()
        {
            InitializeComponent();
            FormClosing += new FormClosingEventHandler(FormMain_FormClosing);
            listBoxRobots.MouseDoubleClick += new MouseEventHandler(listBoxRobots_MouseDoubleClick);
        }

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            Data d = new Data();
            d.DebugMode = checkBoxDebugMode.Checked;
            d.WriteLogOutput = checkBoxWriteLog.Checked;
            d.TurnDurationMs = (int)numericTurnTime.Value;
            d.ForcedRandomSeed = checkBoxForcedSeed.Checked;
            d.RandomSeed = (int)numericRandomSeed.Value;
            d.Robots = mRobots;
            String content = JsonConvert.SerializeObject(d);
            File.WriteAllText(Application.StartupPath + @"\snapshot.json", content);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (File.Exists(Application.StartupPath + @"\snapshot.json"))
            {
                String content = File.ReadAllText(Application.StartupPath + @"\snapshot.json");
                Data d = JsonConvert.DeserializeObject<Data>(content);
                if (d != null)
                {
                    checkBoxDebugMode.Checked = d.DebugMode;
                    checkBoxWriteLog.Checked = d.WriteLogOutput;
                    numericTurnTime.Value = d.TurnDurationMs;
                    checkBoxForcedSeed.Checked = d.ForcedRandomSeed;
                    numericRandomSeed.Value = d.RandomSeed;
                    mRobots = new List<Robot>(d.Robots);
                }
            }
            listBoxRobots.DataSource = null;
            listBoxRobots.DataSource = mRobots;
        }

        private void checkBoxForcedSeed_CheckedChanged(object sender, EventArgs e)
        {
            if (sender is CheckBox)
            {
                CheckBox cb = sender as CheckBox;
                numericRandomSeed.ReadOnly = !cb.Checked;
                numericRandomSeed.Enabled = cb.Checked;
            }
        }

        private void buttonLaunch_Click(object sender, EventArgs e)
        {
            if (!File.Exists(Application.StartupPath + @"\mrc-runner.exe"))
            {
                MessageBox.Show(Application.StartupPath + @"\mrc-runner.exe file not found!");
                return;
            }
            String args = "";
            if (checkBoxDebugMode.Checked)
                args += "-d ";
            if (checkBoxWriteLog.Checked)
                args += "-o battle-<TIME>.log ";
            args += "-t " + numericTurnTime.Value.ToString() + " ";
            if (checkBoxForcedSeed.Checked)
                args += "-s " + numericRandomSeed.Value.ToString() + " ";
            foreach (Robot r in mRobots)
                args += "-r " + r.ToString() + " ";
            ProcessStartInfo info = new ProcessStartInfo(Application.StartupPath + @"\mrc-runner.exe", args);
            info.CreateNoWindow = false;
            info.UseShellExecute = false;
            info.WorkingDirectory = Application.StartupPath;
            if (Process.Start(info) != null)
                Close();
        }

        private void buttonAddRobot_Click(object sender, EventArgs e)
        {
            FormRobot fr = new FormRobot(this);
            DialogResult dr = fr.ShowDialog();
            if(dr == DialogResult.OK && fr.Robot != null)
            {
                if (String.IsNullOrEmpty(fr.Robot.File))
                    mRobots.Remove(fr.Robot);
                else
                    mRobots.Add(fr.Robot);
                listBoxRobots.DataSource = null;
                listBoxRobots.DataSource = mRobots;
            }
        }

        private void buttonRemoveRobot_Click(object sender, EventArgs e)
        {
            if (listBoxRobots.SelectedItem != null)
            {
                mRobots.Remove(listBoxRobots.SelectedItem as Robot);
                listBoxRobots.DataSource = null;
                listBoxRobots.DataSource = mRobots;
            }
        }

        private void listBoxRobots_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int i = listBoxRobots.IndexFromPoint(e.Location);
            if (i != ListBox.NoMatches)
            {
                Robot r = mRobots[i];
                FormRobot fr = new FormRobot(this, r);
                DialogResult dr = fr.ShowDialog();
                if (dr == DialogResult.OK && fr.Robot != null)
                {
                    mRobots[i] = fr.Robot;
                    listBoxRobots.DataSource = null;
                    listBoxRobots.DataSource = mRobots;
                }
            }
        }        
    }
}
