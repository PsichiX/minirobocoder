﻿namespace MiniRoboCoderLauncher
{
    partial class FormRobot
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxFile = new System.Windows.Forms.TextBox();
            this.buttonSelect = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.numericRandomXmin = new System.Windows.Forms.NumericUpDown();
            this.radioButtonRandomX = new System.Windows.Forms.RadioButton();
            this.numericX = new System.Windows.Forms.NumericUpDown();
            this.radioButtonX = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.numericRandomXmax = new System.Windows.Forms.NumericUpDown();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.numericRandomYmax = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.numericRandomYmin = new System.Windows.Forms.NumericUpDown();
            this.radioButtonRandomY = new System.Windows.Forms.RadioButton();
            this.numericY = new System.Windows.Forms.NumericUpDown();
            this.radioButtonY = new System.Windows.Forms.RadioButton();
            this.buttonConfirm = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericRandomXmin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericRandomXmax)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericRandomYmax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericRandomYmin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericY)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxFile
            // 
            this.textBoxFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxFile.Location = new System.Drawing.Point(6, 19);
            this.textBoxFile.Name = "textBoxFile";
            this.textBoxFile.Size = new System.Drawing.Size(175, 20);
            this.textBoxFile.TabIndex = 1;
            // 
            // buttonSelect
            // 
            this.buttonSelect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSelect.Location = new System.Drawing.Point(187, 18);
            this.buttonSelect.Name = "buttonSelect";
            this.buttonSelect.Size = new System.Drawing.Size(75, 23);
            this.buttonSelect.TabIndex = 2;
            this.buttonSelect.Text = "Select";
            this.buttonSelect.UseVisualStyleBackColor = true;
            this.buttonSelect.Click += new System.EventHandler(this.buttonSelect_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.textBoxFile);
            this.groupBox1.Controls.Add(this.buttonSelect);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(268, 50);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Program file";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.numericRandomXmax);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.numericRandomXmin);
            this.groupBox2.Controls.Add(this.radioButtonRandomX);
            this.groupBox2.Controls.Add(this.numericX);
            this.groupBox2.Controls.Add(this.radioButtonX);
            this.groupBox2.Location = new System.Drawing.Point(12, 69);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(268, 192);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Start X position";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 95);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Minimum:";
            // 
            // numericRandomXmin
            // 
            this.numericRandomXmin.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.numericRandomXmin.Enabled = false;
            this.numericRandomXmin.Location = new System.Drawing.Point(7, 117);
            this.numericRandomXmin.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.numericRandomXmin.Minimum = new decimal(new int[] {
            4,
            0,
            0,
            -2147483648});
            this.numericRandomXmin.Name = "numericRandomXmin";
            this.numericRandomXmin.ReadOnly = true;
            this.numericRandomXmin.Size = new System.Drawing.Size(255, 20);
            this.numericRandomXmin.TabIndex = 3;
            this.numericRandomXmin.Value = new decimal(new int[] {
            4,
            0,
            0,
            -2147483648});
            // 
            // radioButtonRandomX
            // 
            this.radioButtonRandomX.AutoSize = true;
            this.radioButtonRandomX.Location = new System.Drawing.Point(7, 71);
            this.radioButtonRandomX.Name = "radioButtonRandomX";
            this.radioButtonRandomX.Size = new System.Drawing.Size(94, 17);
            this.radioButtonRandomX.TabIndex = 2;
            this.radioButtonRandomX.TabStop = true;
            this.radioButtonRandomX.Text = "Random value";
            this.radioButtonRandomX.UseVisualStyleBackColor = true;
            this.radioButtonRandomX.CheckedChanged += new System.EventHandler(this.radioButtonRandomX_CheckedChanged);
            // 
            // numericX
            // 
            this.numericX.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.numericX.Location = new System.Drawing.Point(7, 44);
            this.numericX.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.numericX.Minimum = new decimal(new int[] {
            4,
            0,
            0,
            -2147483648});
            this.numericX.Name = "numericX";
            this.numericX.Size = new System.Drawing.Size(255, 20);
            this.numericX.TabIndex = 1;
            // 
            // radioButtonX
            // 
            this.radioButtonX.AutoSize = true;
            this.radioButtonX.Checked = true;
            this.radioButtonX.Location = new System.Drawing.Point(7, 20);
            this.radioButtonX.Name = "radioButtonX";
            this.radioButtonX.Size = new System.Drawing.Size(98, 17);
            this.radioButtonX.TabIndex = 0;
            this.radioButtonX.TabStop = true;
            this.radioButtonX.Text = "Specified value";
            this.radioButtonX.UseVisualStyleBackColor = true;
            this.radioButtonX.CheckedChanged += new System.EventHandler(this.radioButtonX_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 145);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Maximum:";
            // 
            // numericRandomXmax
            // 
            this.numericRandomXmax.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.numericRandomXmax.Enabled = false;
            this.numericRandomXmax.Location = new System.Drawing.Point(7, 161);
            this.numericRandomXmax.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.numericRandomXmax.Minimum = new decimal(new int[] {
            4,
            0,
            0,
            -2147483648});
            this.numericRandomXmax.Name = "numericRandomXmax";
            this.numericRandomXmax.ReadOnly = true;
            this.numericRandomXmax.Size = new System.Drawing.Size(255, 20);
            this.numericRandomXmax.TabIndex = 6;
            this.numericRandomXmax.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.numericRandomYmax);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.numericRandomYmin);
            this.groupBox3.Controls.Add(this.radioButtonRandomY);
            this.groupBox3.Controls.Add(this.numericY);
            this.groupBox3.Controls.Add(this.radioButtonY);
            this.groupBox3.Location = new System.Drawing.Point(12, 267);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(268, 192);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Start Y position";
            // 
            // numericRandomYmax
            // 
            this.numericRandomYmax.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.numericRandomYmax.Enabled = false;
            this.numericRandomYmax.Location = new System.Drawing.Point(7, 161);
            this.numericRandomYmax.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.numericRandomYmax.Minimum = new decimal(new int[] {
            4,
            0,
            0,
            -2147483648});
            this.numericRandomYmax.Name = "numericRandomYmax";
            this.numericRandomYmax.ReadOnly = true;
            this.numericRandomYmax.Size = new System.Drawing.Size(255, 20);
            this.numericRandomYmax.TabIndex = 6;
            this.numericRandomYmax.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 145);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Maximum:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 95);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Minimum:";
            // 
            // numericRandomYmin
            // 
            this.numericRandomYmin.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.numericRandomYmin.Enabled = false;
            this.numericRandomYmin.Location = new System.Drawing.Point(7, 117);
            this.numericRandomYmin.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.numericRandomYmin.Minimum = new decimal(new int[] {
            4,
            0,
            0,
            -2147483648});
            this.numericRandomYmin.Name = "numericRandomYmin";
            this.numericRandomYmin.ReadOnly = true;
            this.numericRandomYmin.Size = new System.Drawing.Size(255, 20);
            this.numericRandomYmin.TabIndex = 3;
            this.numericRandomYmin.Value = new decimal(new int[] {
            4,
            0,
            0,
            -2147483648});
            // 
            // radioButtonRandomY
            // 
            this.radioButtonRandomY.AutoSize = true;
            this.radioButtonRandomY.Location = new System.Drawing.Point(7, 71);
            this.radioButtonRandomY.Name = "radioButtonRandomY";
            this.radioButtonRandomY.Size = new System.Drawing.Size(94, 17);
            this.radioButtonRandomY.TabIndex = 2;
            this.radioButtonRandomY.TabStop = true;
            this.radioButtonRandomY.Text = "Random value";
            this.radioButtonRandomY.UseVisualStyleBackColor = true;
            this.radioButtonRandomY.CheckedChanged += new System.EventHandler(this.radioButtonRandomY_CheckedChanged);
            // 
            // numericY
            // 
            this.numericY.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.numericY.Location = new System.Drawing.Point(7, 44);
            this.numericY.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.numericY.Minimum = new decimal(new int[] {
            4,
            0,
            0,
            -2147483648});
            this.numericY.Name = "numericY";
            this.numericY.Size = new System.Drawing.Size(255, 20);
            this.numericY.TabIndex = 1;
            // 
            // radioButtonY
            // 
            this.radioButtonY.AutoSize = true;
            this.radioButtonY.Checked = true;
            this.radioButtonY.Location = new System.Drawing.Point(7, 20);
            this.radioButtonY.Name = "radioButtonY";
            this.radioButtonY.Size = new System.Drawing.Size(98, 17);
            this.radioButtonY.TabIndex = 0;
            this.radioButtonY.TabStop = true;
            this.radioButtonY.Text = "Specified value";
            this.radioButtonY.UseVisualStyleBackColor = true;
            this.radioButtonY.CheckedChanged += new System.EventHandler(this.radioButtonY_CheckedChanged);
            // 
            // buttonConfirm
            // 
            this.buttonConfirm.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonConfirm.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonConfirm.Location = new System.Drawing.Point(13, 466);
            this.buttonConfirm.Name = "buttonConfirm";
            this.buttonConfirm.Size = new System.Drawing.Size(267, 23);
            this.buttonConfirm.TabIndex = 8;
            this.buttonConfirm.Text = "Confirm";
            this.buttonConfirm.UseVisualStyleBackColor = true;
            this.buttonConfirm.Click += new System.EventHandler(this.buttonConfirm_Click);
            // 
            // FormRobot
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 499);
            this.Controls.Add(this.buttonConfirm);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.MinimumSize = new System.Drawing.Size(300, 526);
            this.Name = "FormRobot";
            this.Text = "Edit robot";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericRandomXmin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericRandomXmax)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericRandomYmax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericRandomYmin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericY)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxFile;
        private System.Windows.Forms.Button buttonSelect;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericRandomXmin;
        private System.Windows.Forms.RadioButton radioButtonRandomX;
        private System.Windows.Forms.NumericUpDown numericX;
        private System.Windows.Forms.RadioButton radioButtonX;
        private System.Windows.Forms.NumericUpDown numericRandomXmax;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.NumericUpDown numericRandomYmax;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown numericRandomYmin;
        private System.Windows.Forms.RadioButton radioButtonRandomY;
        private System.Windows.Forms.NumericUpDown numericY;
        private System.Windows.Forms.RadioButton radioButtonY;
        private System.Windows.Forms.Button buttonConfirm;
    }
}