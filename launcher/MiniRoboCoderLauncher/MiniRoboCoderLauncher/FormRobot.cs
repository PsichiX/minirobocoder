﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace MiniRoboCoderLauncher
{
    public partial class FormRobot : Form
    {
        private FormMain mFormMain;
        private Robot mRobot;

        public FormRobot(FormMain formMain, Robot robot = null)
        {
            InitializeComponent();
            mFormMain = formMain;
            mRobot = new Robot(robot);
            textBoxFile.Text = mRobot.File;
            numericX.Value = mRobot.StartX;
            numericY.Value = mRobot.StartY;
            numericRandomXmin.Value = mRobot.StartRandomXmin;
            numericRandomXmax.Value = mRobot.StartRandomXmax;
            numericRandomYmin.Value = mRobot.StartRandomYmin;
            numericRandomYmax.Value = mRobot.StartRandomYmax;
            radioButtonX.Checked = !mRobot.StartRandomXmode;
            radioButtonY.Checked = !mRobot.StartRandomYmode;
            radioButtonRandomX.Checked = mRobot.StartRandomXmode;
            radioButtonRandomY.Checked = mRobot.StartRandomYmode;
        }

        public Robot Robot
        {
            get
            {
                return mRobot;
            }
        }

        private void radioButtonRandomX_CheckedChanged(object sender, EventArgs e)
        {
            if(sender is RadioButton)
            {
                RadioButton rb = sender as RadioButton;
                numericRandomXmin.ReadOnly = !rb.Checked;
                numericRandomXmax.ReadOnly = !rb.Checked;
                numericRandomXmin.Enabled = rb.Checked;
                numericRandomXmax.Enabled = rb.Checked;
            }
        }

        private void radioButtonX_CheckedChanged(object sender, EventArgs e)
        {
            if (sender is RadioButton)
            {
                RadioButton rb = sender as RadioButton;
                numericX.ReadOnly = !rb.Checked;
                numericX.Enabled = rb.Checked;
            }
        }

        private void radioButtonRandomY_CheckedChanged(object sender, EventArgs e)
        {
            if (sender is RadioButton)
            {
                RadioButton rb = sender as RadioButton;
                numericRandomYmin.ReadOnly = !rb.Checked;
                numericRandomYmax.ReadOnly = !rb.Checked;
                numericRandomYmin.Enabled = rb.Checked;
                numericRandomYmax.Enabled = rb.Checked;
            }
        }

        private void radioButtonY_CheckedChanged(object sender, EventArgs e)
        {
            if (sender is RadioButton)
            {
                RadioButton rb = sender as RadioButton;
                numericY.ReadOnly = !rb.Checked;
                numericY.Enabled = rb.Checked;
            }
        }

        private void buttonConfirm_Click(object sender, EventArgs e)
        {
            mRobot.File = textBoxFile.Text;
            mRobot.StartX = (int)numericX.Value;
            mRobot.StartY = (int)numericY.Value;
            mRobot.StartRandomXmin = (int)numericRandomXmin.Value;
            mRobot.StartRandomXmax = (int)numericRandomXmax.Value;
            mRobot.StartRandomYmin = (int)numericRandomYmin.Value;
            mRobot.StartRandomYmax = (int)numericRandomYmax.Value;
            mRobot.StartRandomXmode = radioButtonRandomX.Checked;
            mRobot.StartRandomYmode = radioButtonRandomY.Checked;
            DialogResult = DialogResult.OK;
        }

        private void buttonSelect_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.InitialDirectory = @"robots\";
            ofd.Filter = "Intuicio Script file (*.isc)|*.isc|Intuicio Program file (*.itc)|*.itc|Known Intuicio files (*.isc; *.itc)|*.isc;*.itc";
            ofd.FilterIndex = 3;
            DialogResult dr = ofd.ShowDialog();
            if (dr == DialogResult.OK)
                textBoxFile.Text = Utils.MakeRelativePath(Application.StartupPath + @"\robots\", ofd.FileName);
        }
    }
}
