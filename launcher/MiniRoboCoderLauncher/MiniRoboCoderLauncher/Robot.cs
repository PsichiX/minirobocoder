﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MiniRoboCoderLauncher
{
    public class Robot
    {
        public String File;
        public int StartX = 0;
        public int StartY = 0;
        public int StartRandomXmin = -4;
        public int StartRandomXmax = 4;
        public int StartRandomYmin = -4;
        public int StartRandomYmax = 4;
        public bool StartRandomXmode = false;
        public bool StartRandomYmode = false;

        public Robot()
        {
        }

        public Robot(Robot copy)
        {
            if (copy == null)
                return;
            File = String.Copy(copy.File);
            StartX = copy.StartX;
            StartY = copy.StartY;
            StartRandomXmin = copy.StartRandomXmin;
            StartRandomXmax = copy.StartRandomXmax;
            StartRandomYmin = copy.StartRandomYmin;
            StartRandomYmax = copy.StartRandomYmax;
            StartRandomXmode = copy.StartRandomXmode;
            StartRandomYmode = copy.StartRandomYmode;
        }

        public Robot(String n, int x, int y)
        {
            File = n;
            StartX = x;
            StartY = y;
            StartRandomXmode = false;
            StartRandomYmode = false;
        }

        public Robot(String n, int minX, int minY, int maxX, int maxY)
        {
            File = n;
            StartRandomXmin = minX;
            StartRandomYmin = minY;
            StartRandomXmax = maxX;
            StartRandomYmax = maxY;
            StartRandomXmode = true;
            StartRandomYmode = true;
        }

        public override string ToString()
        {
            return File + ";"
                + (StartRandomXmode ? (StartRandomXmin + "<>" + StartRandomXmax) : StartX.ToString()) + ";"
                + (StartRandomYmode ? (StartRandomYmin + "<>" + StartRandomYmax) : StartY.ToString());
        }
    }
}
