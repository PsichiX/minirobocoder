﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace MiniRoboCoderLauncher
{
    public class Utils
    {
        /// <summary>
        /// Extract relative path between two files or directories.
        /// </summary>
        /// <param name="fromPath">Path to translate from.</param>
        /// <param name="toPath">Path to translate to.</param>
        /// <returns>Relative path.</returns>
        public static String MakeRelativePath(String fromPath, String toPath)
        {
            if (String.IsNullOrEmpty(fromPath)) throw new ArgumentNullException("fromPath");
            if (String.IsNullOrEmpty(toPath)) throw new ArgumentNullException("toPath");
            Uri fromUri = new Uri(fromPath);
            Uri toUri = new Uri(toPath);
            if (fromUri.Scheme != toUri.Scheme) // path can't be made relative.
                return toPath;
            Uri relativeUri = fromUri.MakeRelativeUri(toUri);
            String relativePath = Uri.UnescapeDataString(relativeUri.ToString());
            if (toUri.Scheme.ToUpperInvariant() == "FILE")
                relativePath = relativePath.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
            return relativePath;
        }
    }
}
