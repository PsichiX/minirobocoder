#include <windows.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <list>
#include <ctime>
#include <SFML/Graphics.hpp>
#include <XeCore/Intuicio/IntuicioVM.h>
#include <IntuicioCompiler.h>
#include "RobotInterceptions.h"
#include "Robot.h"
#include "Bullet.h"
#include "Explosion.h"
#include "Animation.h"
#include "Utils.h"

#define SUCCESS return EXIT_SUCCESS;
#define FAILURE { system( "pause" ); return EXIT_FAILURE; }
#define OUTPUT if( g_output ) (*g_output)
#define OUTPUT_FLUSH if( g_output ) g_output->flush();

using namespace std;
using namespace XeCore;
using namespace XeCore::Common;
using namespace XeCore::Intuicio;

int g_arenaCols = 9;
int g_arenaRows = 9;
int g_turnTimeMs = 1000;
sf::Font* g_font = 0;
sf::Texture* g_textureArena = 0;
sf::Texture* g_textureRobot = 0;
sf::Texture* g_textureBullet = 0;
sf::Texture* g_textureExplosion = 0;
ofstream* g_output;

int main( int argc, char* argv[] )
{
    String argstr;
    for( int i = 1; i < argc; i++ )
        argstr += String( i > 0 ? " " : "" ) + String( argv[ i ] );
    #ifdef DEBUG
    argstr = "-t 100 -r testA.isc;-4<>4;-4 -r testB.isc;-4<>4;4 -o sim-<TIME>.log";
    #endif

    bool debugMode = false;
    unsigned int seed = time( 0 );
    vector< string > robotsConfigs;
    string output;
    time_t currentTime = time( 0 );
    struct tm* currentTimeInfo = localtime( &currentTime );
    char currentTimeBuff[ 129 ];
    strftime( currentTimeBuff, 129, "%Y-%m-%d-%H-%M-%S", currentTimeInfo );
    string currentTimeStr( currentTimeBuff );
    cout << "Startup time: " << currentTimeStr << " (timestamp: " << currentTime << ")" << endl;

    bool argMode = true;
    int argType = 0;
    unsigned int argsc = 0;
    String* args = argstr.explode( " |\t", argsc, false, '|' );
    for( unsigned int i = 0; i < argsc; i++ )
    {
        if( argMode )
        {
            if( args[ i ].length() > 1 && args[ i ].at( 0 ) == '-' )
            {
                args[ i ].assign( args[ i ].substr( 1 ) );
                if( args[ i ] == "d" || args[ i ] == "debug" )
                    debugMode = true;
                else if( args[ i ] == "r" || args[ i ] == "robot" )
                {
                    argType = 1;
                    argMode = false;
                }
                else if( args[ i ] == "s" || args[ i ] == "seed" )
                {
                    argType = 2;
                    argMode = false;
                }
                else if( args[ i ] == "o" || args[ i ] == "output" )
                {
                    argType = 3;
                    argMode = false;
                }
                else if( args[ i ] == "t" || args[ i ] == "time" )
                {
                    argType = 4;
                    argMode = false;
                }
            }
        }
        else
        {
            if( argType == 1 )
            {
                robotsConfigs.push_back( args[ i ] );
                argType = 0;
                argMode = true;
            }
            else if( argType == 2 )
            {
                args[ i ].convertTo< unsigned int >( seed );
                argType = 0;
                argMode = true;
            }
            else if( argType == 3 )
            {
                output = args[ i ];
                string strToReplace = "<TIME>";
                unsigned int p = output.find( strToReplace );
                while( p != string::npos )
                {
                    output.replace( p, strToReplace.length(), currentTimeStr );
                    p = output.find( strToReplace );
                }
                argType = 0;
                argMode = true;
            }
            else if( argType == 4 )
            {
                args[ i ].convertTo< int >( g_turnTimeMs );
                g_turnTimeMs = max( 0, g_turnTimeMs );
                argType = 0;
                argMode = true;
            }
        }
    }
    DELETE_ARRAY( args );

    if( debugMode )
        cout << "Game is running in Debug Mode!" << endl;
    cout << "Random seed: " << seed << endl;
    randomSeed( seed );
    g_output = xnew ofstream( output.c_str() );
    if( !g_output || !*g_output )
    {
        cout << "Cannot open output file: " << output << endl;
        DELETE_OBJECT( g_output );
    }
    OUTPUT << "Simulation start: " << currentTimeStr << endl;
    OUTPUT << "Random seed: " << seed << endl;
    OUTPUT << "Turn time ms: " << g_turnTimeMs << endl;
    OUTPUT << "Arena size: " << g_arenaCols << "x" << g_arenaRows << endl;
    OUTPUT_FLUSH;

    sf::RenderWindow window( sf::VideoMode( 800, 600 ), "MiniRoboCoder" );
    float aspect = (float)window.getSize().x / (float)window.getSize().y;

    sf::View worldCamera( sf::Vector2f( 0.0f, 0.0f ), sf::Vector2f( aspect, 1.0f ) );
    worldCamera.zoom( 10.0f );

    sf::View guiCamera( sf::Vector2f( 0.0f, 0.0f ), sf::Vector2f( aspect, 1.0f ) );
    guiCamera.zoom( 1024.0f );

    g_font = xnew sf::Font();
    if( !g_font->loadFromFile( "resources/CONTF.ttf" ) )
    {
        cout << "Cannot load font!" << endl;
        FAILURE;
    }

    g_textureArena = xnew sf::Texture();
    if( !g_textureArena->loadFromFile( "resources/arena.png" ) )
    {
        cout << "Cannot load arena texture!" << endl;
        DELETE_OBJECT( g_font );
        DELETE_OBJECT( g_textureArena );
        FAILURE;
    }
    g_textureArena->setSmooth( true );
    g_textureArena->setRepeated( true );

    g_textureRobot = xnew sf::Texture();
    if( !g_textureRobot->loadFromFile( "resources/robot.png" ) )
    {
        cout << "Cannot load robot texture!" << endl;
        DELETE_OBJECT( g_font );
        DELETE_OBJECT( g_textureArena );
        DELETE_OBJECT( g_textureRobot );
        FAILURE;
    }
    g_textureRobot->setSmooth( true );
    g_textureRobot->setRepeated( false );

    g_textureBullet = xnew sf::Texture();
    if( !g_textureBullet->loadFromFile( "resources/bullet.png" ) )
    {
        cout << "Cannot load bullet texture!" << endl;
        DELETE_OBJECT( g_font );
        DELETE_OBJECT( g_textureArena );
        DELETE_OBJECT( g_textureRobot );
        DELETE_OBJECT( g_textureBullet );
        FAILURE;
    }
    g_textureBullet->setSmooth( true );
    g_textureBullet->setRepeated( false );

    g_textureExplosion = xnew sf::Texture();
    if( !g_textureExplosion->loadFromFile( "resources/explosion.png" ) )
    {
        cout << "Cannot load explosion texture!" << endl;
        DELETE_OBJECT( g_font );
        DELETE_OBJECT( g_textureArena );
        DELETE_OBJECT( g_textureRobot );
        DELETE_OBJECT( g_textureBullet );
        DELETE_OBJECT( g_textureExplosion );
        FAILURE;
    }
    g_textureExplosion->setSmooth( true );
    g_textureExplosion->setRepeated( false );

    IntuicioVM* vm = xnew IntuicioVM();
    map< string, Robot* > robots;
    list< Bullet* > bullets;
    list< Explosion* > explosions;
    std::list< Animation* > animations;
    for( unsigned int i = 0; i < robotsConfigs.size(); i++ )
    {
        Robot::Config conf = Robot::Config::fromString( robotsConfigs[ i ] );
        Robot* robot = xnew Robot(
            conf.file,
            vm,
            robots,
            sf::Vector2i( conf.startX, conf.startY ),
            (float)random( 0, 4 ) * 90.0f
        );
        if( robot->isValid() )
        {
            if( !robots.count( conf.file ) )
            {
                robots[ conf.file ] = robot;
                cout << "Robot loaded: " << conf.file << endl;
                OUTPUT << "Loaded robot config: '" << conf.toString() << "'" << endl;
            }
            else
            {
                cout << "Robot already exists: " << conf.file << endl;
                DELETE_OBJECT( robot );
            }
        }
        else
        {
            cout << "Cannot create robot: " << conf.file << endl;
            DELETE_OBJECT( robot );
        }
    }
    robotsConfigs.clear();
    OUTPUT_FLUSH;

    sf::RectangleShape* arena = xnew sf::RectangleShape();
    arena->setTexture( g_textureArena );
    arena->setSize( sf::Vector2f( (float)g_arenaCols, (float)g_arenaRows ) );
    arena->setTextureRect( sf::IntRect( 0, 0, g_textureArena->getSize().x * 10, g_textureArena->getSize().y * 10 ) );
    arena->setOrigin( arena->getSize().x * 0.5f, arena->getSize().y * 0.5f );
    sf::IntRect worldRect = sf::IntRect( -( ( g_arenaCols - 1 ) / 2 ), -( ( g_arenaRows - 1 ) / 2 ), g_arenaCols - 1, g_arenaRows - 1 );
    sf::RectangleShape* overlay = xnew sf::RectangleShape();
    overlay->setFillColor( sf::Color( 0, 0, 0, 128 ) );
    overlay->setSize( guiCamera.getSize() );
    overlay->setOrigin( overlay->getSize().x * 0.5f, overlay->getSize().y * 0.5f );
    sf::Text* textOverlay = xnew sf::Text();
    textOverlay->setFont( *g_font );
    textOverlay->setCharacterSize( 128 );
    textOverlay->setStyle( sf::Text::Bold );
    textOverlay->setString( "Press space to play/pause" );
    sf::FloatRect rect = textOverlay->getLocalBounds();
    textOverlay->setOrigin( rect.width * 0.5f, rect.height * 0.5f );
    sf::Text* textWinner = xnew sf::Text();
    textWinner->setFont( *g_font );
    textWinner->setCharacterSize( 128 );
    textWinner->setStyle( sf::Text::Bold );

    for( map< string, Robot* >::iterator it = robots.begin(); it != robots.end(); it++ )
        if( it->second )
            it->second->processStart();

    unsigned int turn = 0;
    bool turnChanged = true;
    unsigned int turnPhase = 0;
    unsigned int robotTurn = 0;
    bool simulationComplete = false;
    bool simulationRunning = false;
    Robot* winner = 0;
    try
    {
        sf::Clock clock;
        sf::Clock animClock;
        while( window.isOpen() )
        {
            sf::Event event;
            while( window.pollEvent( event ) )
            {
                if( event.type == sf::Event::Closed )
                    window.close();
                else if( event.type == sf::Event::Resized )
                {
                    float aspect = (float)event.size.width / (float)event.size.height;
                    worldCamera.setSize( sf::Vector2f( aspect, 1.0f ) );
                    worldCamera.zoom( 10.0f );
                    guiCamera.setSize( sf::Vector2f( aspect, 1.0f ) );
                    guiCamera.zoom( 1024.0f );
                }
                else if( event.type == sf::Event::KeyPressed )
                {
                    if( event.key.code == sf::Keyboard::Escape )
                        window.close();
                    else if( event.key.code == sf::Keyboard::Space && !simulationComplete )
                        simulationRunning = !simulationRunning;
                }
            }

            if( robots.size() < 2 )
            {
                if( robots.size() == 1 )
                {
                    winner = robots.begin()->second;
                    textOverlay->setString( "The winner is:" );
                    sf::FloatRect rect = textOverlay->getLocalBounds();
                    textOverlay->setOrigin( rect.width * 0.5f, rect.height + 50.0f );
                    textWinner->setString( winner->name() );
                    rect = textWinner->getLocalBounds();
                    textWinner->setOrigin( rect.width * 0.5f, -50.0f );
                }
                else
                {
                    winner = 0;
                    textOverlay->setString( "There is no winner!" );
                    sf::FloatRect rect = textOverlay->getLocalBounds();
                    textOverlay->setOrigin( rect.width * 0.5f, rect.height * 0.5f );
                    textWinner->setString( "" );
                }
                simulationRunning = false;
                simulationComplete = true;
            }
            if( simulationRunning && clock.getElapsedTime().asMilliseconds() > g_turnTimeMs )
            {
                if( turnChanged )
                {
                    OUTPUT << "Turn: " << turn << endl;
                    OUTPUT << "Robots: " << robots.size() << endl;
                    OUTPUT << "Bullets: " << bullets.size() << endl;
                    turn++;
                    turnChanged = false;
                }
                turnPhase %= 2;
                if( turnPhase == 0 && robots.empty() )
                    turnPhase++;
                if( turnPhase == 1 && bullets.empty() )
                    turnPhase++;
                turnPhase %= 2;
                unsigned int i = 0;
                if( turnPhase == 0 )
                {
                    for( map< string, Robot* >::iterator it = robots.begin(); it != robots.end(); it++, i++ )
                    {
                        Robot* r = it->second;
                        if( !r )
                            continue;
                        if( i == robotTurn )
                        {
                            OUTPUT << "Process robot: " << it->second << "; file: " << r->name() << endl;
                            r->processStep( worldRect, animations );
                            sf::Vector2i sp = r->getSourcePosition();
                            sf::Vector2i tp = r->getTargetPosition();
                            OUTPUT << "Move from: " << sp.x << "x" << sp.y << endl;
                            OUTPUT << "Move to: " << tp.x << "x" << tp.y << endl;
                            OUTPUT << "Direction: " << (int)r->getDirection() << endl;
                            OUTPUT << "Is alive: " << r->isAlive() << endl;
                            OUTPUT << "Is shooting: " << r->canShoot() << endl;
                            OUTPUT << "Shields: " << r->getShieldsCount() << endl;
                            OUTPUT << "Bullets: " << r->getBulletsCount() << endl;
                            if( !r->isAlive() )
                            {
                                string rn = it->first;
                                it++;
                                robots.erase( rn );
                                DELETE_OBJECT( r );
                                robotTurn--;
                                i--;
                            }
                            else
                            {
                                if( r->canShoot() )
                                {
                                    Bullet* b = xnew Bullet(
                                        r,
                                        robots,
                                        r->getForwardPosition(),
                                        r->getDirection()
                                    );
                                    bullets.push_back( b );
                                    r->shoot();
                                }
                            }
                        }
                    }
                    robotTurn++;
                    if( robotTurn >= robots.size() )
                    {
                        robotTurn = 0;
                        turnPhase++;
                        turnChanged = true;
                    }
                }
                else if( turnPhase == 1 )
                {
                    for( list< Bullet* >::iterator it = bullets.begin(); it != bullets.end(); it++ )
                    {
                        Bullet* b = *it;
                        if( !b )
                            continue;
                        OUTPUT << "Process bullet: " << *it << endl;
                        sf::Vector2i sp = b->getPosition();
                        b->processStep( worldRect, animations );
                        sf::Vector2i tp = b->getPosition();
                        OUTPUT << "Move from: " << sp.x << "x" << sp.y << endl;
                        OUTPUT << "Move to: " << tp.x << "x" << tp.y << endl;
                        OUTPUT << "Direction: " << (int)b->getDirection() << endl;
                        OUTPUT << "Is alive: " << b->isAlive() << endl;
                        if( !b->isAlive() )
                        {
                            Explosion* exp = xnew Explosion( sp, g_turnTimeMs * (float)0.001f );
                            explosions.push_back( exp );
                            it++;
                            bullets.remove( b );
                            DELETE_OBJECT( b );
                        }
                    }
                    turnPhase++;
                    turnChanged = true;
                }
                clock.restart();
                OUTPUT_FLUSH;
            }
            for( list< Animation* >::iterator it = animations.begin(); it != animations.end(); it++ )
            {
                Animation* a = *it;
                if( !a )
                    continue;
                a->delay -= animClock.getElapsedTime().asSeconds();
                a->apply();
                if( a->delay <= -a->duration )
                {
                    it++;
                    animations.remove( a );
                    DELETE_OBJECT( a );
                }
            }
            for( list< Explosion* >::iterator it = explosions.begin(); it != explosions.end(); it++ )
            {
                Explosion* exp = *it;
                if( !exp )
                    continue;
                exp->animate( animClock.getElapsedTime().asSeconds() );
                if( exp->isComplete() )
                {
                    it++;
                    explosions.remove( exp );
                    DELETE_OBJECT( exp );
                }
            }
            animClock.restart();

            window.clear( sf::Color( 0, 0, 0, 255 ) );

            window.setView( worldCamera );
            window.draw( *arena );
            for( map< string, Robot* >::iterator it = robots.begin(); it != robots.end(); it++ )
            {
                Robot* r = it->second;
                if( r )
                {
                    window.draw( *r->getShape() );
                    if( debugMode )
                        window.draw( *r->getShapeDebug() );
                }
            }
            for( list< Bullet* >::iterator it = bullets.begin(); it != bullets.end(); it++ )
            {
                Bullet* b = *it;
                if( b )
                {
                    window.draw( *b->getShape() );
                    if( debugMode )
                        window.draw( *b->getShapeDebug() );
                }
            }
            for( list< Explosion* >::iterator it = explosions.begin(); it != explosions.end(); it++ )
            {
                Explosion* exp = *it;
                if( exp )
                {
                    window.draw( *exp->getShape() );
                    if( debugMode )
                        window.draw( *exp->getShapeDebug() );
                }
            }

            window.setView( guiCamera );
            for( map< string, Robot* >::iterator it = robots.begin(); it != robots.end(); it++ )
            {
                Robot* r = it->second;
                if( r )
                {
                    sf::Vector2f p = worldCamera.getTransform().transformPoint( r->getShape()->getPosition() - r->getShape()->getOrigin() );
                    p = guiCamera.getInverseTransform().transformPoint( p );
                    r->getText()->setPosition( p );
                    window.draw( *r->getText() );
                }
            }
            if( !simulationRunning )
            {
                window.draw( *overlay );
                window.draw( *textOverlay );
                if( simulationComplete )
                    window.draw( *textWinner );
            }

            window.display();
            Sleep( 1000 / 60 );
        }
    }
    catch( ... )
    {
        cout << "Unexpected exception!" << endl;
    }

    for( map< string, Robot* >::iterator it = robots.begin(); it != robots.end(); it++ )
    {
        if( it->second )
        {
            it->second->processStop();
            DELETE_OBJECT( it->second );
        }
    }
    robots.clear();
    for( list< Bullet* >::iterator it = bullets.begin(); it != bullets.end(); it++ )
    {
        if( *it )
        {
            DELETE_OBJECT( *it );
        }
    }
    bullets.clear();
    for( list< Explosion* >::iterator it = explosions.begin(); it != explosions.end(); it++ )
    {
        if( *it )
        {
            DELETE_OBJECT( *it );
        }
    }
    explosions.clear();
    for( list< Animation* >::iterator it = animations.begin(); it != animations.end(); it++ )
    {
        if( *it )
        {
            DELETE_OBJECT( *it );
        }
    }
    animations.clear();

    OUTPUT << "Robots: " << robots.size() << endl;
    OUTPUT << "Bullets: " << bullets.size() << endl;
    OUTPUT << "Simulaton stop" << endl;
    OUTPUT_FLUSH;
    DELETE_OBJECT( g_output );
    DELETE_OBJECT( g_font );
    DELETE_OBJECT( g_textureArena );
    DELETE_OBJECT( g_textureRobot );
    DELETE_OBJECT( arena );
    DELETE_OBJECT( overlay );
    DELETE_OBJECT( textOverlay );
    DELETE_OBJECT( textWinner );
    DELETE_OBJECT( vm );

    SUCCESS;
}
