#ifndef __ROBOTS_HELPER__
#define __ROBOTS_HELPER__

#inject "robots.isc"

!registers-i 8

// go to given point.
%FUNCTION_VOID( goTo, int x, int y )%
	
	!data int cx 0
	!data int cy 0
	%GET_X( $cx )%
	%GET_Y( $cy )%
	
	!data int dx 0
	movi regi:0 $x
	movi regi:1 $cx
	subi 2 0 1
	movi $dx regi:2
	!data int adx 0
	absi 2 2
	movi $adx regi:2
	
	!data int dy 0
	movi regi:0 $y
	movi regi:1 $cy
	subi 2 0 1
	movi $dy regi:2
	!data int ady 0
	absi 2 2
	movi $ady regi:2
	
	!data int zero 0
	movi regi:0 $zero
	movi regi:1 $adx
	movi regi:2 $ady	
	tlti 3 1 2
	jifi 3 @processByY @processByX
	!jump processByX
		movi regi:1 $dx
		tlti 4 1 0
		jifi 4 @goLeft @goLeftElse
		!jump goLeft
			%GO_LEFT%
		!jump goLeftElse
		tgti 4 1 0
		jifi 4 @goRight @goRightElse
		!jump goRight
			%GO_RIGHT%
		!jump goRightElse
		goto @endFunc
	!jump processByY
		movi regi:1 $dy
		tlti 4 1 0
		jifi 4 @goUp @goUpElse
		!jump goUp
			%GO_UP%
		!jump goUpElse
		tgti 4 1 0
		jifi 4 @goDown @goDownElse
		!jump goDown
			%GO_DOWN%
		!jump goDownElse
	!jump endFunc
	
%ENDFUNCTION%

// find nearest enemy ID.
%FUNCTION( int nearestEnemy )%
	
	!data int cx 0
	!data int cy 0
	!data int count 0
	%GET_X( $cx )%
	%GET_Y( $cy )%
	%GET_ENEMIES_COUNT( $count )%
	
	!data int idFound -1
	!data int distFound -1
	!data int zero 0
	movi regi:0 $zero
	movi regi:1 $count
	!jump loopStart
		tlti 2 0 1
		jifi 2 @loopStep @loopEnd
		!jump loopStep
			!data int epx 0
			!data int epy 0
			%GET_ENEMY_X( $epx, regi:0 )%
			%GET_ENEMY_Y( $epy, regi:0 )%
			
			movi regi:3 $epx
			movi regi:4 $cx
			subi 5 3 4
			absi 5 5
			movi regi:3 $epy
			movi regi:4 $cy
			subi 6 3 4
			absi 6 6
			addi 6 5 6
			
			movi regi:3 $distFound
			movi regi:4 $zero
			tlti 5 3 4
			jifi 5 @setFound @setFoundElse
			!jump setFound
				movi $idFound regi:0
				movi $distFound regi:6
				goto @setFoundEnd
			!jump setFoundElse
				tlti 5 6 3
				jifi 5 @setFound @setFoundEnd
			!jump setFoundEnd
			
			inci 0 0
		goto @loopStart
	!jump loopEnd
	
	%RETURN( $idFound )%
	
%ENDFUNCTION%

// check if robot has clear sight at point.
%FUNCTION( int hasClearSight, int x, int y )%
	
	!data int cx 0
	!data int cy 0
	!data int dir 0
	%GET_X( $cx )%
	%GET_Y( $cy )%
	%GET_DIR( $dir )%
	!data int dx 0
	!data int dy 0
	movi regi:0 $x
	movi regi:1 $cx
	subi 2 0 1
	movi $dx regi:2
	movi regi:0 $y
	movi regi:1 $cy
	subi 2 0 1
	movi $dy regi:2
	
	!data int zero 0
	!data int one 1
	movi regi:1 $dir
	movi regi:2 $dx
	movi regi:3 $dy
	movi regi:4 $zero
	
	movi regi:0 $DIRECTION_LEFT
	teti 5 0 1
	tlti 6 2 4
	and 5 5 6
	teti 6 3 4
	and 5 5 6
	jifi 5 @hasSightLeft @hasNoSightLeft
	!jump hasSightLeft
		%RETURN( $one )%
	!jump hasNoSightLeft
	
	movi regi:0 $DIRECTION_UP
	teti 5 0 1
	teti 6 2 4
	and 5 5 6
	tlti 6 3 4
	and 5 5 6
	jifi 5 @hasSightUp @hasNoSightUp
	!jump hasSightUp
		%RETURN( $one )%
	!jump hasNoSightUp
	
	movi regi:0 $DIRECTION_RIGHT
	teti 5 0 1
	tgti 6 2 4
	and 5 5 6
	teti 6 3 4
	and 5 5 6
	jifi 5 @hasSightRight @hasNoSightRight
	!jump hasSightRight
		%RETURN( $one )%
	!jump hasNoSightRight
	
	movi regi:0 $DIRECTION_DOWN
	teti 5 0 1
	teti 6 2 4
	and 5 5 6
	tgti 6 3 4
	and 5 5 6
	jifi 5 @hasSightDown @hasNoSightDown
	!jump hasSightDown
		%RETURN( $one )%
	!jump hasNoSightDown
	
	%RETURN( $zero )%
	
%ENDFUNCTION%

#endif
